package com.techprimers.spring_soap_example.endpoint;

import com.techprimers.spring_soap_example.GetAsistentRequest;
import com.techprimers.spring_soap_example.GetAsistentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import com.techprimers.spring_soap_example.service.AsistentService;

@Endpoint
public class AsistentEndpoint {

    @Autowired
    private AsistentService asistentService;
    @PayloadRoot(namespace = "http://techprimers.com/spring_soap_example",localPart = "getAsistentRequest")
    @ResponsePayload
    public GetAsistentResponse getAsistentResponse(@RequestPayload GetAsistentRequest request){
        GetAsistentResponse response = new GetAsistentResponse();
        response.setAsistent(asistentService.getAsistent(request.getName()));
        return response;
    }
}
