package com.techprimers.spring_soap_example.service;

import com.techprimers.spring_soap_example.Asistent;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Service
public class AsistentService {

    private static final Map<String, Asistent> asistent=new HashMap<>();


    @PostConstruct
    public void initialize(){
        Asistent diana=new Asistent();
        Asistent ioana=new Asistent();
        diana.setName("Diana");
        diana.setAdress("Sighisoarei");
        diana.setDate("25/23/23");
        diana.setGen("F");
        diana.setRol("Asistent");
        diana.setUsername("Diana");
        diana.setPassword("Diana");

        ioana.setName("ioana");
        ioana.setAdress("Sighisoarei");
        ioana.setDate("25/23/23");
        ioana.setGen("F");
        ioana.setRol("Asistent");
        ioana.setUsername("ioana");
        ioana.setPassword("ioana");
        asistent.put(diana.getName(),diana);
        asistent.put(ioana.getName(),ioana);
    }
    public Asistent getAsistent(String name){
        return asistent.get(name);
    }
}
