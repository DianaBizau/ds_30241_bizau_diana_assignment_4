package com.techprimers.spring_soap_example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSoapApplication {
    public static void main(String[] args){
        SpringApplication.run(SpringBootSoapApplication.class,args);
    }
}
